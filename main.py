import string
import random

def password_Gen(length=12, upper=False, lower=False, punc=False, digit=False):
    pool = ''
    #length = int(input("Please Enter Your Length: " ))
    if upper:
        pool += string.ascii_uppercase
    
    if lower:
        pool += string.ascii_lowercase

    if punc:
        pool += string.punctuation

    if digit:
        pool += string.digits

    #str(print(random.sample(pool, k=length)))
    print(''.join(random.sample(pool, k=length)))

password_Gen(upper=True, lower=True, punc=True, digit=True)



